""" Test the integration for portfolio project
"""
import os
import pytest
#import requests

PORTFOLIO_HOSTNAME = os.environ.get('PORTFOLIO_HOSTNAME', 'portfolio')

@pytest.mark.integration
def test_noop():
    """ Just a demonstration of a test in this module,
        since the others require some more thought in having
        istio allow gitlab runner non-mtls traffic to this service
    """


#@pytest.mark.integration
#def test_health_check():
#    """ Checks the health of the service """
#    reply = requests.get(f"http://{PORTFOLIO_HOSTNAME}/health")
#    assert reply.status_code == 200
#    assert reply.json() == dict(healthy=True)
#
#@pytest.mark.integration
#def test_portfolio():
#    """ Make sure that portfolio queries return results. Note, that
#        this exercises the ticker service as an external dependency.
#    """
#    response = requests.get(f"http://{PORTFOLIO_HOSTNAME}/user1/balance")
#    assert response.status_code == 200
