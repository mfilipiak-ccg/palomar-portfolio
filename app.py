"""
User's holdings
"""
import os
import time
import requests

from flask import Flask, jsonify, abort, request


class SessionTracing(requests.sessions.Session): # pylint: disable=too-few-public-methods
    """ Hooking in requests to provide tracing """

    def __init__(self, tracer=None, propagate=True, span_tags=None): # pylint: disable=unused-argument
        super(SessionTracing, self).__init__()

    def request(self, method, url, *args, **kwargs):
        """ Hooks the request to add tracing """
        headers = kwargs.setdefault('headers', {})
        for key, val in request.headers.items():
            low_key = key.lower()
            if low_key.startswith('x-b3'):
                headers[key] = val
        return super(SessionTracing, self).request(method, url, *args,
                                                   **kwargs)


APP = Flask(__name__)

TICKER_HOSTNAME = os.environ.get('TICKER_HOSTNAME', 'ticker.palomar')

# These are static users and their holdings
# The ticker will be some variance around this
USERS_HOLDINGS = dict(user1=dict(a=3, b=10, c=60),
                      user2=dict(b=30, ),
                      user3=dict(a=1, c=3))


def calculate_balance(user_holdings):
    """ Calculates the balance based on the holdings and current price from the ticker """
    balance = 0

    session = SessionTracing()
    for company, units in user_holdings.items():
        url = f"http://{TICKER_HOSTNAME}/{company}/ticker"

        response = session.get(url, timeout=3)
        if response.status_code != 200:
            print('Error querying ticker service', response.reason)
            raise Exception("Error retrieving ticker information")

        ticker_info = response.json()

        # Note: the eager usage of the external API here
        balance += ticker_info['price'] * units

    # truncate the balance to 2 digits
    return int(balance * 100) / 100

@APP.route('/<user>/balance', methods=['GET'])
def portfolio_balance(user):
    """ API for the balance """
    user_holdings = USERS_HOLDINGS.get(user)
    if not user_holdings:
        abort(404)

    balance = calculate_balance(user_holdings)  #, request.headers)

    return jsonify(dict(balance=balance, user=user, epoch_time=time.time()))

@APP.route('/health')
def health_check():
    """ Health API """
    return jsonify({'healthy': True})
