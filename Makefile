img_name = palomar-portfolio
img_version = 0.1

build:
	docker build -t $(img_name):$(img_version) .

deploy:
	helm upgrade -i -n palomar $(img_name) chart

deploy-standalone:
	helm upgrade -i -n palomar $(img_name) chart --set istio_enabled=false

undeploy:
	helm delete -n palomar $(img_name) ||:

import-k3d:
	k3d import-images $(img_name):$(img_version)

update-k3d: build import-k3d deploy

force-update-k3d: undeploy update-k3d

force-update-standalone: undeploy build import-k3d deploy-standalone
